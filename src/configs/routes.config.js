import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import React, { Suspense, lazy } from "react";

import { AuthProvider } from "../contexts/authContext";
import { PageLoading } from "../components/loading";
import { getLocalStorage } from "../utils/storage";

const RequireAuth = ({ children }) => {
  const location = useLocation();

  return getLocalStorage("access_token") ? (
    children
  ) : (
    <Navigate to="/auth/login" state={{ from: location }} />
  );
};

const delayRoute = (ms = 500) => {
  return (promise) =>
    promise.then(
      (data) =>
        new Promise((resolve) => {
          setTimeout(() => resolve(data), ms);
        })
    );
};

/**
 * Public Route
 */

const registerPage = {
  path: "/auth/register",
  component: lazy(() =>
    delayRoute()(import("../modules/auth/features/register"))
  ),
};

const loginPage = {
  path: "/auth/login",
  name: "login",
  component: lazy(() => delayRoute()(import("../modules/auth/features/login"))),
};

/**
 * Private Route
 */
const dashboard = {
  path: "/",
  name: "Dasboard",
  component: lazy(() =>
    delayRoute()(import("../modules/dasboard/features/main"))
  ),
};

const publicRoutesData = [registerPage, loginPage];
const privateRoutesData = [dashboard];

const PublicRoutes = () => {
  return publicRoutesData.map((route, index) => {
    const { component: Component, path, ...rest } = route;
    return (
      <Route
        {...rest}
        key={`public-route-${index}`}
        path={path}
        element={
          <Suspense fallback={<PageLoading />}>
            <Component />
          </Suspense>
        }
        exact
      />
    );
  });
};

const PrivateRoutes = () => {
  return privateRoutesData.map((route, index) => {
    const { component: Component, path, ...rest } = route;
    return (
      <Route
        {...rest}
        key={`private-route-${index}`}
        path={path}
        element={
          <Suspense fallback={<PageLoading />}>
            <RequireAuth>
              <Component />
            </RequireAuth>
          </Suspense>
        }
      />
    );
  });
};

const AppRoutes = () => {
  return (
    <BrowserRouter>
      <AuthProvider>
        <Routes>
          {PublicRoutes()}
          {PrivateRoutes()}
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default AppRoutes;
