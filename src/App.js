import "react-toastify/dist/ReactToastify.css";

import { QueryClient, QueryClientProvider } from "react-query";

import AppRoutes from "./configs/routes.config";
import { Auth0Provider } from "@auth0/auth0-react";
import React from "react";
import { ToastContainer } from "react-toastify";
import { getConfig } from "./configs/getConfig";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 0,
      refetchOnWindowFocus: false,
    },
    mutations: {
      // mutation options
    },
  },
});
function App() {
  const { apiUrl, clientId } = getConfig();

  return (
    <QueryClientProvider client={queryClient}>
      <Auth0Provider
        domain={apiUrl}
        clientId={clientId}
        redirectUri={window.location.origin}
        cacheLocation="localstorage"
      >
        <AppRoutes />
        <ToastContainer />
      </Auth0Provider>
    </QueryClientProvider>
  );
}

export default App;
