import { Button, Col, Container, Row } from "react-bootstrap";

import React from "react";
import { getLocalStorage } from "../../../../utils/storage";
import { useAuth } from "../../../../contexts/authContext";

export default function Dasboards() {
  const { logout, profile } = useAuth();

  return (
    <Container>
      <Row style={{ marginTop: 50 }}>
        <Col md={{ span: 6, offset: 3 }}>
          <div>
            <img src={profile?.picture} alt={profile?.name} />
            <h2>{profile?.name}</h2>
            <p>{profile?.email}</p>
            <p>
              <b>Token:</b> {getLocalStorage("access_token")}
            </p>
            <Button onClick={() => logout()}>Logout</Button>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
