import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import LoginForm from "../../components/loginForm";
import React from "react";
import Row from "react-bootstrap/Row";
import { displaySuccessMessage } from "../../../../utils/request";
import { useForm } from "react-hook-form";
import useLogin from "../../services/useLogin";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const { mutateAsync: login, isLoading } = useLogin();
  const navigate = useNavigate();

  const onSubmit = async (data) => {
    try {
      login(data, {
        onSuccess: () => {
          navigate("/");
          displaySuccessMessage("Login Success");
        },
      });
    } catch (error) {}
  };

  return (
    <Container>
      <Row style={{ marginTop: 50 }}>
        <Col md={{ span: 6, offset: 3 }}>
          <LoginForm
            onSubmit={handleSubmit(onSubmit)}
            error={errors}
            register={register}
            isLoading={isLoading}
          />
        </Col>
      </Row>
    </Container>
  );
}
