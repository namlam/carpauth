import { getConfig } from "../../../configs/getConfig";
import request from "../../../utils/request";
import { useMutation } from "react-query";

const { clientId } = getConfig();

const useRegister = (options) =>
  useMutation(
    async (data) =>
      request.post("/dbconnections/signup", {
        ...data,
        client_id: clientId,
        connection: "Username-Password-Authentication",
      }),
    {
      ...options,
      onSuccess: (r) => {
        options && options.onSuccess(r);
      },
    }
  );

export default useRegister;
