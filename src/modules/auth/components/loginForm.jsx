import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import React from "react";
import { useNavigate } from "react-router";

export default function LoginForm({ onSubmit, register, isLoading }) {
  const navigate = useNavigate();

  return (
    <>
      <h1>Login</h1>
      <Form onSubmit={onSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            required
            {...register("username")}
            type="email"
            placeholder="Enter Email"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            required
            {...register("password")}
            type="password"
            placeholder="Password"
          />
        </Form.Group>

        <Button disabled={isLoading} variant="primary" type="submit">
          Submit
        </Button>

        <Button variant="link" onClick={() => navigate("/auth/register")}>
          Register
        </Button>
      </Form>
    </>
  );
}
