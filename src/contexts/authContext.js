import React, { createContext, useCallback, useContext, useMemo } from "react";
import { getLocalStorage, removeLocalStorage } from "../utils/storage";

import useGetProfile from "../modules/auth/services/useGetProfile";
import { useNavigate } from "react-router";

export const AuthContext = createContext(null);
export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  const navigate = useNavigate();
  const { data } = useGetProfile({
    enabled: !!getLocalStorage("access_token"),
  });

  const onLogout = useCallback(() => {
    removeLocalStorage("access_token");
    removeLocalStorage("token_id");
    navigate("/");
  }, [navigate]);

  const contextValue = useMemo(
    () => ({
      profile: data,
      logout: onLogout,
    }),
    [data, onLogout]
  );

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};

export default AuthContext;
